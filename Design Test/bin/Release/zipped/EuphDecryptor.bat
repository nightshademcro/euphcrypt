@echo off
title EuphDecryptor
cls
color c
setlocal enableDelayedExpansion

set /P NUMCODE=Number Code:
set /p TEXT=Text:

set code=%TEXT%
echo Decrypting: %TEXT%

set "chars=1234567891abcdefghijklmnopqrstuvwxyz"

for /l %%N in (10 1 36) do (
    for /f %%C in ("!chars:~%%N,1!") do (
        SET /A MATH=%%N*%NUMCODE%

        for /f %%F in ("!MATH!") do (
            set "code=!code:-%%F=%%C!"
        )
    )
)

echo !code! >> decrypt.txt
start decrypt.txt
timeout 1 /nobreak >nul
del decrypt.txt
timeout 15 /nobreak >nul
del decrypt.txt
taskkill notepad.exe
pause
