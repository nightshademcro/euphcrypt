@echo off
title EuphEncryptor
cls
color c
setlocal enableDelayedExpansion
set /P NUMCODE=Number Code:
set /p TEXT=Text:
echo %TEXT%
set code=%TEXT%
echo Encrypting: %TEXT%

set "chars=1234567891abcdefghijklmnopqrstuvwxyz"

for /l %%N in (10 1 36) do (
    for /f %%C in ("!chars:~%%N,1!") do (
        SET /A MATH=%%N*%NUMCODE%

        for /f %%F in ("!MATH!") do (
            set "code=!code:%%C=-%%F!"
        )
    )
)
echo !code! >> crypt.txt
echo|set/p=!code!|clip
echo Copied the code to clipboard.
echo It will self destruct in 15 seconds.
start crypt.txt
timeout 1 /nobreak >nul
del crypt.txt
timeout 15 /nobreak >nul
echo Successfully Self-Destructed > crypt.txt
taskkill /im notepad.exe >nul
pause
