﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Aes_Example;

namespace Design_Test
{
    public partial class Form1 : Form
    {

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void richTextBox1_TextChanged_1(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            Process.Start(@"D:\Documents\VSCODE\Batch\Stock\Stock.bat");
        }

        private void richTextBox1_TextChanged_2(object sender, EventArgs e)
        {

        }

        private void button1_Click_2(object sender, EventArgs e)
        {
            this.UseWaitCursor=true;
            String message1 = textBox1.Text;
            String ckey = keyTextBox.Text;
            byte[] buffer = System.Text.Encoding.UTF8.GetBytes(ckey.PadLeft(32));
            using (Aes myAes = Aes.Create())
            {

                // Encrypt the string to an array of bytes.
                byte[] encrypted = AesExample.EncryptStringToBytes_Aes(message1, buffer, new byte[16]);

                string encstrng = BitConverter.ToString(encrypted).ToLower();
                outputEnc.Text = encstrng;

                //MessageBox.Show(message1);
                //MessageBox.Show(encstrng);
                //MessageBox.Show(BitConverter.ToString(buffer).Replace("-", "").ToLower()); 
                this.UseWaitCursor = false;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            String ckey = keyTextBox.Text;
            byte[] ckeyby = System.Text.Encoding.UTF8.GetBytes(ckey.PadLeft(32));
            string encrypted = inputDec.Text;
                        
            byte[] buffer = encrypted.Split('-')
                .Select(x => byte.Parse(x, NumberStyles.HexNumber))
                .ToArray();
            string roundtrip = AesExample.DecryptStringFromBytes_Aes(buffer, ckeyby, new byte[16]);
            outputDec.Text = roundtrip;
        }

        private void button3_Click(object sender, EventArgs e)
        {
        }

        private void button4_Click(object sender, EventArgs e)
        {
        }

        private void button3_Click_1(object sender, EventArgs e)
        {
            MessageBox.Show("EuphEncryptor, created by Rever#8828 on Discord.", "Credits");
        }

        private void textBox1_TextChanged_1(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void inputDec_TextChanged(object sender, EventArgs e)
        {

        }

        private void outputDec_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
