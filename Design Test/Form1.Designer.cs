﻿
namespace Design_Test
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.EncryptButton = new System.Windows.Forms.Button();
            this.DecryptButton = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.keyTextBox = new System.Windows.Forms.TextBox();
            this.outputEnc = new System.Windows.Forms.RichTextBox();
            this.inputDec = new System.Windows.Forms.TextBox();
            this.outputDec = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // EncryptButton
            // 
            this.EncryptButton.BackgroundImage = global::Design_Test.Properties.Resources.background;
            this.EncryptButton.Font = new System.Drawing.Font("Steezy", 21.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.EncryptButton.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.EncryptButton.Location = new System.Drawing.Point(12, 297);
            this.EncryptButton.Name = "EncryptButton";
            this.EncryptButton.Size = new System.Drawing.Size(176, 64);
            this.EncryptButton.TabIndex = 0;
            this.EncryptButton.Text = "Encrypt";
            this.EncryptButton.UseVisualStyleBackColor = true;
            this.EncryptButton.Click += new System.EventHandler(this.button1_Click_2);
            // 
            // DecryptButton
            // 
            this.DecryptButton.AccessibleName = "EuphEncryptor";
            this.DecryptButton.BackgroundImage = global::Design_Test.Properties.Resources.background;
            this.DecryptButton.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.DecryptButton.Font = new System.Drawing.Font("Steezy", 21.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DecryptButton.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.DecryptButton.Location = new System.Drawing.Point(401, 297);
            this.DecryptButton.Name = "DecryptButton";
            this.DecryptButton.Size = new System.Drawing.Size(176, 64);
            this.DecryptButton.TabIndex = 1;
            this.DecryptButton.Text = "Decrypt";
            this.DecryptButton.UseVisualStyleBackColor = true;
            this.DecryptButton.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.BackgroundImage = global::Design_Test.Properties.Resources.background;
            this.button3.Font = new System.Drawing.Font("Steezy", 21.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button3.Location = new System.Drawing.Point(209, 297);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(176, 64);
            this.button3.TabIndex = 2;
            this.button3.Text = "Credits";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click_1);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(12, 271);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(176, 20);
            this.textBox1.TabIndex = 3;
            this.textBox1.Text = "Message To Encrypt";
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged_1);
            // 
            // keyTextBox
            // 
            this.keyTextBox.Location = new System.Drawing.Point(12, 12);
            this.keyTextBox.Name = "keyTextBox";
            this.keyTextBox.Size = new System.Drawing.Size(176, 20);
            this.keyTextBox.TabIndex = 4;
            this.keyTextBox.Text = "Key";
            this.keyTextBox.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            // 
            // outputEnc
            // 
            this.outputEnc.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.outputEnc.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.outputEnc.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.outputEnc.Font = new System.Drawing.Font("MS UI Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.outputEnc.ForeColor = System.Drawing.SystemColors.ScrollBar;
            this.outputEnc.Location = new System.Drawing.Point(209, 12);
            this.outputEnc.Name = "outputEnc";
            this.outputEnc.Size = new System.Drawing.Size(368, 46);
            this.outputEnc.TabIndex = 5;
            this.outputEnc.Text = "Message After Encryption";
            // 
            // inputDec
            // 
            this.inputDec.Location = new System.Drawing.Point(401, 271);
            this.inputDec.Name = "inputDec";
            this.inputDec.Size = new System.Drawing.Size(176, 20);
            this.inputDec.TabIndex = 6;
            this.inputDec.Text = "Message To Decrypt";
            this.inputDec.TextChanged += new System.EventHandler(this.inputDec_TextChanged);
            // 
            // outputDec
            // 
            this.outputDec.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.outputDec.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.outputDec.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.outputDec.Font = new System.Drawing.Font("MS UI Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.outputDec.ForeColor = System.Drawing.SystemColors.ScrollBar;
            this.outputDec.Location = new System.Drawing.Point(209, 64);
            this.outputDec.Name = "outputDec";
            this.outputDec.Size = new System.Drawing.Size(368, 46);
            this.outputDec.TabIndex = 7;
            this.outputDec.Text = "Message After Decryption";
            this.outputDec.TextChanged += new System.EventHandler(this.outputDec_TextChanged);
            // 
            // Form1
            // 
            this.AccessibleName = "EuphEncryption/Decryption";
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.Color.DarkRed;
            this.BackgroundImage = global::Design_Test.Properties.Resources.maxresdefault;
            this.ClientSize = new System.Drawing.Size(589, 373);
            this.Controls.Add(this.outputDec);
            this.Controls.Add(this.inputDec);
            this.Controls.Add(this.outputEnc);
            this.Controls.Add(this.keyTextBox);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.DecryptButton);
            this.Controls.Add(this.EncryptButton);
            this.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.DoubleBuffered = true;
            this.ForeColor = System.Drawing.Color.Yellow;
            this.HelpButton = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Euph";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button EncryptButton;
        private System.Windows.Forms.Button DecryptButton;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox keyTextBox;
        private System.Windows.Forms.RichTextBox outputEnc;
        private System.Windows.Forms.TextBox inputDec;
        private System.Windows.Forms.RichTextBox outputDec;
    }
}

